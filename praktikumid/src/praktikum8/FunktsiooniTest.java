package praktikum8;

import java.util.Random;

public class FunktsiooniTest {
    public static void main(String[] args) {

        // Näitamaks, et h(k) = [m(k*T - [k*T])] paiskfunktsioon EI sobi kimbumeetodil sorteerimiseks
        // Selle asemel kasutage hoopis
        // h(k) = (int) (n*(k-a)/(k-b))
        // kus n = paisktabeli suurus, a=arvuvahemiku väikseim arv, b = arvuvahemiku maksimum +1
        
        Random r = new Random();
        for(int i=0;i<100;i++){
            int elem1 = Math.abs(r.nextInt() % 100000);
            int elem2 = Math.abs(r.nextInt() % 100000);
            int indeks1 = paiska(elem1);
            int indeks2 = paiska(elem2);
            boolean yksArvSuurem = false;
            boolean yksIndeksSuurem = false;
            if (elem1 >= elem2){
                yksArvSuurem = true;
            }
            if (indeks1 >= indeks2){
                yksIndeksSuurem = true;
            }

            if(yksArvSuurem && !yksIndeksSuurem){
                System.out.println("---- Erinevus! ----");
                System.out.println(elem1 + " ; " + elem2);
                System.out.println(indeks1 + " ; "+indeks2);
            }
        }
    }

    public static int paiska(int n){
        double T = (Math.sqrt(5) - 1)/2;
        int m = 1000;
        return (int) (m*(n*T - ((int) (n*T))));
    }
}
