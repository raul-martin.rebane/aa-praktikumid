package praktikum8;

// Kuna tegu on javafxi nõudva klassiga, siis jätan ta välja kommenteerituna.
/*
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.stage.Stage;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;


public class PaiskamiseVisualiseerimine extends Application {
    @Override public void start(Stage stage) {

        final NumberAxis yAxis2 = new NumberAxis();
        final CategoryAxis xAxis2 = new CategoryAxis();
        final BarChart<String,Number> barChart = new BarChart<>(xAxis2,yAxis2);

        XYChart.Series series1 = new XYChart.Series();

        barChart.getData().add(series1);

        // Paiskame 1000 juhuslikku kirjet ning loeme üle, kui mitu korda me oleme iga indeksi saanud
        Random r = new Random();
        HashMap<Integer,Integer> loendur = new HashMap<>();
        for(int i=0;i <= 1000; i+= 1){
            // Meie juhuslik kirje
            int k = r.nextInt();
            // Paiskame "tabelisse" mille suurus on 100
            int indeks = paiska(k,100);
            // Suurendame vastava indeksi põrgete arvu
            int porkeid = loendur.getOrDefault(indeks,0);
            loendur.put(indeks,porkeid+1);
        }

        // Kuvame graafiku, kus x teljel on indeksid ning y teljel on vastavuses kui mitu korda me üritasime elementi
        // sinna indeksile panna
        for(Map.Entry<Integer,Integer> porkeEntry : loendur.entrySet()){
            series1.getData().add(new XYChart.Data(Integer.toString(porkeEntry.getKey()),porkeEntry.getValue()));
        }
        Scene scene  = new Scene(barChart,800,600);

        stage.setScene(scene);
        stage.show();
    }

    // Int -> Int
    public static int paiska(int n, int m){
        double T = (Math.sqrt(5) - 1)/2;
        return (int) (m*(n*T - ((int) (n*T))));
    }


    public static void main(String[] args) {
        launch(args);
    }
}

*/