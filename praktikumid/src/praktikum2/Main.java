package praktikum2;

// Välja kommenteeritud selleks, et terve projekt ei nõuaks ilmtingimata javaFX'i.
// Kui soovite seda ka käima saada, järgige juhiseid lehel
// https://openjfx.io/openjfx-docs/#IDE-Intellij

/*
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class Main extends Application {
    int kordusi = 10;
    int rekAndmemaht = 40;
    int iterAndmemaht = 40;

    @Override public void start(Stage stage) {
        stage.setTitle("Line Chart Sample");
        //defining the axes
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Andmemaht");
        yAxis.setLabel("Tööaeg");
        //creating the chart
        final LineChart<Number,Number> lineChart =
                new LineChart<Number,Number>(xAxis,yAxis);

        lineChart.setTitle("Kodutöö 1");
        //defining a series
        XYChart.Series rekursiivne = new XYChart.Series();
        rekursiivne.setName("Rekursiivne Fibonacci");
        //populating the series with data
        for (int i = 1; i < rekAndmemaht; i++) {
            rekursiivne.getData().add(new XYChart.Data(i,mooda(i,"rekursiivne")));
        }

        XYChart.Series iteratiivne = new XYChart.Series();
        iteratiivne.setName("Iteratiivne Fibonacci");
        for(int i=1; i<iterAndmemaht; i++){
            iteratiivne.getData().add(new XYChart.Data(i,mooda(i,"iteratiivne")));
        }

        Scene scene  = new Scene(lineChart,800,600);
        lineChart.getData().add(rekursiivne);
        lineChart.getData().add(iteratiivne);

        Random r = new Random(System.currentTimeMillis());
        System.out.println(genereeriMassiiv(20,r));
        System.out.println(genereeriMassiiv(20,r));
        stage.setScene(scene);
        stage.show();
    }

    private int mooda(int n,String algoritm){
        Random random = new Random(System.currentTimeMillis());
        int kokku = 0;
        for (int i = 0; i < kordusi; i++) {
            List<Integer> andmed = genereeriMassiiv(n,random);

            long start = System.currentTimeMillis();
            if(algoritm.equals("rekursiivne")){
                Fibo.fibo(n);
            }
            if(algoritm.equals("iteratiivne")){
                Fibo.fiboIter(n);
            }
            long lopp = System.currentTimeMillis();
            kokku += (int) (lopp-start);
        }
        return kokku/kordusi;
    }

    private List<Integer> genereeriMassiiv(int n, Random r){
        List<Integer> massiiv = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            massiiv.add(r.nextInt());
        }
        return massiiv;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
*/