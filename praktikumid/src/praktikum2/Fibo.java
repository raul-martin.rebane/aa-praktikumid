package praktikum2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Fibo {
    public static int fibo(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        return fibo(n - 1) + fibo(n - 2);
    }
    public static int fiboIter(int n){
        List<Integer> vahearvud = new ArrayList<>(Arrays.asList(0,1));
        while (vahearvud.size() - 1 < n){
            int eelviimane = vahearvud.get(vahearvud.size()-1);
            int yleelviimane = vahearvud.get(vahearvud.size()-2);
            vahearvud.add(eelviimane+yleelviimane);
        }
        return vahearvud.get(n);
    }
}
