package praktikum11;

import java.util.ArrayList;
import java.util.HashMap;

public class Kuhi<T extends Comparable<T>> {
    protected ArrayList<T> massiiv;

    //Kodutöö 7's on seda vaja
    //See paneb vastavusse elemendid oma asukohaga kuhjas, et oleks võimalik kiiresti otsida ja üles
    //Mullitada elemente. Seda peab ise käsitsi uuendama kui kuhjas toimub muutusi
    //6. kodutöö jaoks "asukohad" välja pole vaja.

    protected HashMap<T,Integer> asukohad;

    public Kuhi() {
        this.massiiv = new ArrayList<>();
        this.asukohad = new HashMap<>();
    }

    public Kuhi(ArrayList<T> massiiv) {
        this.massiiv = massiiv;
        this.asukohad = new HashMap<>();
        for(int i=0;i<massiiv.size();i++){
            asukohad.put(massiiv.get(i),i);
        }
    }

    void vaheta(int i, int j){
        //lisa ka asukohtade vahetus
    }




    /*
    Ülesanne 1:
    Koostada funktsioonid, mis võtavad argumentideks kuhja ja indeksi k ja tagastavad k-nda elemendi
    * vasaku alluva indeksi
    * parema alluva indeksi
    * vanema indeksi
    Kuna me implementeerime seda isendimeetodina, siis võime kuhja argumendi jätta kaasamata.
     */


    private int vasak(int k){
        int vasak = 2*k +1;
        if(vasak >= massiiv.size()){
            return -1;
        }
        return vasak;
    }

    private int parem(int k){
        int parem = 2*k+2;
        if(parem >= massiiv.size()){
            return -1;
        }
        return parem;
    }

    private int vanem(int k){
        if(k==0){
            return -1;
        }
        return (int)((k-1)/2);
    }

    /*
    Ülesanne 2:
    Koostada funktsioonid
    1) Kuhja elemendi mullina ülesviimiseks
    2) Kuhja elemendi mullina allaviimiseks
     */

    private void mullitaYles(int i){
        if(i==0){
            return;
        }
        int vanemaIndeks = vanem(i);
        if(massiiv.get(vanemaIndeks).compareTo(massiiv.get(i)) > 0){
            T temp = massiiv.get(vanemaIndeks);
            massiiv.set(vanemaIndeks,massiiv.get(i));
            massiiv.set(i,temp);
            mullitaYles(vanemaIndeks);
            // 7. kodutöö muudatus
            asukohad.put(massiiv.get(vanemaIndeks),vanemaIndeks);
            asukohad.put(massiiv.get(i),i);
        }
    }

    private void mullitaAlla(int i){
        throw new RuntimeException("Implementeeri mind!");
    }

    /*
    Ülesanne 3:
    Koostada funktsioonid
    1. Etteantud väärtusega elemendi lisamiseks
    2. Väikseima väärtusega elemendi (juurtipu) eemaldamiseks
     */
    public void lisaKirje(T kirje){
        throw new RuntimeException("Implementeeri mind!");
    }

    public T votaJuur(){
        throw new RuntimeException("Implementeeri mind!");
    }

    //Kodutöö 7 jaoks vaja:
    public void uuendaKirje(T kirje){
        /*
        Peaks kasutama "asukohad" mappi selleks et leida üles see kirje
         */
        throw new RuntimeException("Implementeeri mind!");
    }

    /*
    Ülesanne 4:
    Realiseerida meetod järjendi kuhjastamiseks
     */
    public void kuhjasta(){
        // kuhjasta väljas "massiiv" olev list
        throw new RuntimeException("Implementeeri mind!");
    }

    /*
    Ülesanne 5:
    Realiseerida kuhjameetodil sorteerimine
     */
    public static <T extends Comparable<T>> ArrayList<T> sorteeri(ArrayList<T> sisend){
        throw new RuntimeException("Implementeeri mind!");
    }

}
