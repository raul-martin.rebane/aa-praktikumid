package praktikum11;

import praktikum9.Tipp;

import java.util.PriorityQueue;

import static org.junit.Assert.assertEquals;

public class Huffman {
    /*
    Meetod Huffmani algoritmi kasutades teksti kokku pakkimiseks.
    Saab sisendiks sõne ning tagastab SonePaar objekti, kus
    1) "pakitudTekst" sisaldab pakitud teksti (bittidena nt "01011101")
    2) "koodiPuu" sisaldab Huffmani koodipuu kirjeldust.
        Meil on vaja seda vaja teksti lahti pakkimiseks ning otsene puu struktuur pole vaja alles hoida,
        koodiPuu võib kasutada ka muid sümboleid peale bittide.
     */
    public static SonePaar paki(String tekst){
        // Teed sagedustabeli, siis kuhja tippudest
        // a -> 7, b -> 20
        // Lisasime tipp klassile välja "sagedus" ning tipp elemendid on võrreldavad sageduse pealt
        Tipp<Character> tippa = new Tipp<Character>('a',7);
        Tipp<Character> tippb = new Tipp<Character>('b',20);


        //Siin PriorityQueue asemel peaks olema su oma kuhja implementatsioon.
        PriorityQueue<Tipp<Character>> kuhi = new PriorityQueue<>();
        kuhi.add(tippa);
        kuhi.add(tippb);
        kuhi.add(new Tipp<Character>('u',1));
        System.out.println(kuhi.poll());


        throw new RuntimeException("Implementeeri mind!");
    }

    public static String pakiLahti(String kood, String puuKirjeldus){
        throw new RuntimeException("Implementeeri mind!");
    }

    public static void main(String[] args) {
        /*
        Triviaalselt peab kehtima, et kui me pakime teksti kokku ja lahti, siis saame sama teksti tagasi.
         */
        String sisend = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbccccddddaaaaa";
        SonePaar tulemus = paki(sisend);
        String lahtiKodeeritud = pakiLahti(tulemus.pakitudTekst,tulemus.koodiPuu);
        assertEquals(sisend,lahtiKodeeritud);
        /*
        Järgnevalt võiks kontrollida, et tõesti sagedamalt esinevatele tähtedele omistatakse lühemad prefikskoodid.
         */
    }
}
/*
Kuna javas ei saa me kergelt paari tagastada, siis kasutame selleks abiklassi.
 */
class SonePaar {
    public final String pakitudTekst;
    public final String koodiPuu;

    public SonePaar(String kodeeritudTekst, String koodiPuu) {
        this.pakitudTekst = kodeeritudTekst;
        this.koodiPuu = koodiPuu;
    }
}
