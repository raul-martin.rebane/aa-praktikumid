package praktikum13;

import praktikum12.Lahendaja;

import java.util.Arrays;

public class PraktikumiNaidis {
    public static void main(String[] args) {
        int l = Integer.MAX_VALUE/2;
        int[][] kaugused = {
                {0, 9,  5,  l,   l,   40},
                {9, 0,  l,  3,  l,  l},
                {5,l,0,16,l,10},
                {l,3,16,0,20,12},
                {l,l,l,20,0,l},
                {40,l,10,12,l,0}
        };
        String[] nimed = {"A","B","C","D","E","F"};

        //tähtis: eraldi lahendaja objekt floyd-warshalli jaoks!
        //kui kasutad FW ja siis Dijkstra, siis on Dijkstra tulemus rikutud
        Lahendaja lahendaja = new Lahendaja(kaugused,nimed,l);
        System.out.println(lahendaja.lyhimTeeFloydWarshalliga("A","E"));
        System.out.println(Arrays.toString(lahendaja.kaugused[0]));
        System.out.println(Arrays.toString(lahendaja.kustTulla[0]));
    }
}
