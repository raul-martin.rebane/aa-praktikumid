package praktikum10;
import praktikum6.avaldis.*;

public class Main {

    public static void main(String[] args) {

        // !((AvB)&C)
        LauseArvutuseAvaldis avaldis = new Eitus(
                new Konjunktsioon(
                        new Disjunktsioon(
                                new Muutuja("A"),
                                new Muutuja("B")
                        ),
                        new Muutuja("C")));

        // !A&B&C

        LauseArvutuseAvaldis avaldis2 = new Konjunktsioon(
                new Konjunktsioon(
                        new Eitus(new Muutuja("A")),
                        new Muutuja("B")),
                new Muutuja("C"));

        System.out.println(avaldis);
        System.out.println(avaldis2);

        /*
        sulge pean panema kui:
        1) eitus (v.a. muutujate puhul)
        2) konjunktsioon ja ta laps on disjunktsioon
         */
    }


}
