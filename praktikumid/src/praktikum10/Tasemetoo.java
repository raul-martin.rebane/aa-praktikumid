package praktikum10;

import praktikum9.Tipp;

public class Tasemetoo {
    /*
    5
    |
    5

      10
    5    5


      t.v
     */

    /*

    Tagastab true kui leidub selline alampuu puus
         1
       |
      2
     |
    3

     */

    public boolean keskmine(Tipp<Integer> t, Integer vanemaInfo){
        if(t == null) {
            return false;
        }

        if(t.getInfo() == 2 && vanemaInfo == 1 && t.getVasakAlluv() != null && t.getVasakAlluv().getInfo()==3){
            return true;
        }
        return keskmine(t.getVasakAlluv(),t.getInfo()) || keskmine(t.getParemAlluv(),t.getInfo());
    }

    /* Tagastab true kui leidub jada vanem -> alluv -> alluva alluv, mille infod on järjestikkused arvud */
    public boolean kolmJarjestikust(Tipp<Integer> t, Integer vanemaInfo, Integer jarjestikkuseid){
        if(jarjestikkuseid == 3){
            return true;
        }
        if(t==null){
            return false;
        }
        if(t.getInfo() == vanemaInfo+1){
            jarjestikkuseid++;
        } else {
            jarjestikkuseid = 0;
        }
        return kolmJarjestikust(t.getVasakAlluv(),t.getInfo(),jarjestikkuseid) ||
                kolmJarjestikust(t.getParemAlluv(),t.getInfo(),jarjestikkuseid);

    }



    // Tagastab selliste tippude koguarvu, mille "info" väärtus on võrdne tema laste
    // väärtuste summaga
    public static int fun(Tipp<Integer> t){
        if(t == null){
            return 0;
        }

        // Alati pead olema kindel, et viide pole "null" enne kui kasutad
        int vasakuInfo = t.getVasakAlluv() == null ? 0 : t.getVasakAlluv().getInfo();
        int paremaInfo = t.getParemAlluv() == null ? 0 : t.getParemAlluv().getInfo();
        int tippSobib = paremaInfo + vasakuInfo == t.getInfo() ? 1 : 0;

        return tippSobib + fun(t.getVasakAlluv()) + fun(t.getParemAlluv());
    }


}
