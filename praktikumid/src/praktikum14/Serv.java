package praktikum14;

public class Serv implements Comparable<Serv> {
    Tipp tipp1;
    Tipp tipp2;
    int kaal;

    public Serv(Tipp tipp1, Tipp tipp2, int kaal) {
        this.tipp1 = tipp1;
        this.tipp2 = tipp2;
        this.kaal = kaal;
    }

    public Tipp teineTipp(Tipp t){
        if (t == tipp1){
            return tipp2;
        } else if (t == tipp2){
            return tipp1;
        } throw new RuntimeException("Tipp: "+t+" ei kuulu serva ("+ tipp1 +";"+ tipp2 +";"+kaal);
    }

    @Override
    public String toString() {
        return "("+tipp1+";"+tipp2+";"+kaal+")";
    }

    @Override
    public int compareTo(Serv o) {
        return Integer.compare(kaal,o.kaal);
    }
}
