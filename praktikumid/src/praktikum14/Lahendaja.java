package praktikum14;

import java.io.FileNotFoundException;
import java.util.*;

public class Lahendaja {

    /*
    Järgnevad algoritmid implementeerida koduseks tööks. Sisendgraafi g mitte muuta.
    Tagastada järjend servadest, mis moodustavad minimaalse kaaluga toesepuu.
     */
    public static List<Serv> toesepuuPrimiga(Graaf g){
        //Primi põhiline kuhi, mille põhjal avame uued tipud.
        PriorityQueue<Serv> servadeKuhi = new PriorityQueue<>();
        //Selleks, et oskame öelda, kas serv tekitaks tsükli
        HashSet<Tipp> tipudToeses = new HashSet<>();
        //Siia kogume vastuse
        List<Serv> toes = new ArrayList<>();
        //Lisaoptimeerimine, et mitte lisada ühte serva mitu korda kuhja.
        HashMap<Serv,Boolean> servVaadatud = new HashMap<>();

        //avame tipu:
        //kõik tema väljuvad servad kuhja

        throw new RuntimeException("Implementeeri mind!");
    }

    public static List<Serv> toesepuuKruskaliga(Graaf g){
        HashMap<Tipp,Klassipuu> sidusadKomponendid = new HashMap<>();
        List<Serv> toes = new ArrayList<>();
        throw new RuntimeException("Implementeeri mind!");
    }

    public static void main(String[] args) throws FileNotFoundException {
        Graaf g = naidisGraaf();
        System.out.println(visualiseeri(g));
        System.out.println(g.onPuu());
    }

    public static Graaf naidisGraaf(){
        /*
        See graaf on läbimänguslaididelt moodle lõpust, Primi algoritmi näide.
         */
        Graaf g = new Graaf();
        String[] nimed = {"A","B","C","D","E","F","G","H","I"};
        for(String nimi : nimed){
            g.lisaTipp(nimi);
        }
        g.lisaServ("A","B",3);
        g.lisaServ("A","C",2);
        g.lisaServ("A","D",7);
        g.lisaServ("B","E",5);
        g.lisaServ("B","F",2);
        g.lisaServ("C","G",5);
        g.lisaServ("G","I",2);
        g.lisaServ("F","H",6);
        return g;
    }

    public static String visualiseeri(Graaf g){
        StringBuilder sb = new StringBuilder();
        sb.append("strict graph {\n");
        for(Serv s : g.servad){
            sb.append(s.tipp1+" -- " + s.tipp2+" [label=\""+s.kaal+"\"]\n");
        }
        for(Tipp t : g.tipud.values()){
            if(t.servad.isEmpty()){
                sb.append(t.nimi+"\n");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    public static String visualiseeri(List<Serv> servad){
        StringBuilder sb = new StringBuilder();
        sb.append("strict graph {\n");
        for(Serv s : servad){
            sb.append(s.tipp1+" -- " + s.tipp2+" [label=\""+s.kaal+"\"]\n");
        }
        sb.append("}");
        return sb.toString();
    }
}
