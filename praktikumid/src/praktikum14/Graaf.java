package praktikum14;

import java.io.*;
import java.util.*;

public class Graaf {
    Map<String,Tipp> tipud;
    List<Serv> servad;

    public void lisaTipp(String nimi){
        if(tipud.containsKey(nimi)){
            throw new RuntimeException("Ei saa luua tippu: "+nimi+", selle nimega tipp on juba olemas.");
        }
        Tipp t = new Tipp(nimi);
        tipud.put(nimi,t);
    }

    public void lisaServ(String t1, String t2, int kaal){
        Tipp tipp1 = tipud.get(t1);
        Tipp tipp2 = tipud.get(t2);
        if(tipp1 == null || tipp2 == null){
            String nulline = tipp1 == null ? t1 : t2;
            throw new RuntimeException("Tahad lisada serva "+t1+" ja "+t2+" vahele, aga "+nulline+" pole tippude nimekirjas.");
        }

        Serv s = new Serv(tipp1,tipp2, kaal);
        tipp1.lisaServ(s);
        tipp2.lisaServ(s);
        servad.add(s);
    }

    public Graaf(){
        this.tipud = new HashMap<>();
        this.servad = new ArrayList<>();
    }

    //Kui tahate Eesti kaardil rakendada
    public Graaf(String fail) {
        this.tipud = new HashMap<>();
        this.servad = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(new File(fail)))){
            String[] nimed = br.readLine().split("\t");
            for(String nimi : nimed){
                tipud.put(nimi,new Tipp(nimi));
            }
            String in = br.readLine();
            int i = 0;
            while(in != null){
                String[] reaJupid = in.split("\t");
                for(int j=1;j<reaJupid.length;j++){
                    if(!reaJupid[j].equals("")){
                        this.lisaServ(nimed[i],nimed[j-1],Integer.parseInt(reaJupid[j]));
                    }
                }
                i++;
                in = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean onPuu(){
        //1-tipuline puu
        if(servad.size() == 0){
            return tipud.size()==1;
        }

        LinkedList<Tipp> järjekord = new LinkedList<>();
        järjekord.add(servad.get(0).tipp1);
        HashMap<Tipp,Boolean> käidud = new HashMap<>();
        HashSet<Serv> kasutatudServad = new HashSet<>();

        while (!järjekord.isEmpty()){
            Tipp vaadeldav = järjekord.pollFirst();
            if(käidud.getOrDefault(vaadeldav,false)){
                return false;
            }
            käidud.put(vaadeldav,true);
            for(Serv s : vaadeldav.servad){
                if(!kasutatudServad.contains(s)){
                    kasutatudServad.add(s);
                    järjekord.add(s.teineTipp(vaadeldav));
                }
            }
        }

        for(Tipp t : tipud.values()){
            if(!käidud.containsKey(t)){
                return false;
            }
        }

        return true;
    }
}
