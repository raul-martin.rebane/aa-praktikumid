package praktikum14;

import java.util.ArrayList;
import java.util.List;

public class Tipp {
    String nimi;
    List<Serv> servad;

    public Tipp(String nimi) {
        this.nimi = nimi;
        this.servad = new ArrayList<>();
    }

    public void lisaServ(Serv s){
        if(s == null){
            throw new RuntimeException("Lisad null-kirjet servana tippu"+nimi+"!");
        }
        if(s.tipp1 == this || s.tipp2 == this){
            servad.add(s);
        } else {
            throw new RuntimeException("Serva "+s+" kumbki otspunkt ei ole tipp "+nimi);
        }
    }

    public boolean onNaaber(String t){
        for(Serv s : servad){
            Tipp teine = s.teineTipp(this);
            if(teine.nimi.equals(t)){
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return nimi;
    }
}
