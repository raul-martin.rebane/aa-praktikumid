package praktikum7;

import java.util.Arrays;

public class Paisktabel {

    Isik[] tabel;
    int n;
    int kompesamm;
    int vordlusi;

    public Paisktabel(int n, int kompesamm){
        tabel = new Isik[n];
        this.n = n;
        this.kompesamm = kompesamm;
    }

    void lisaKirje(Isik isik){
        int i = h(isik.ID);

        int samme = 0;
        while (tabel[i] != null){
            i = (i + kompesamm) % this.n;

            samme++;
            if(samme == this.n){
                throw new RuntimeException("Tabel on täis!");
            }
        }
        tabel[i] = isik;
    }

    void eemaldaKirje(Isik isik){
        int i = otsiKirjeIndeks(isik.getID());

        // Ei leidunud kirjet
        if(i == -1){
            return;
        }

        // Kustutame kirje
        tabel[i] = null;
        //Lähme edasi
        i = (i+kompesamm) % this.n;

        //eemaldame ja lisame uuesti kuni auguni
        while(tabel[i] != null){
            Isik praegune = tabel[i];
            tabel[i] = null;
            lisaKirje(praegune);
            i = (i + kompesamm) % this.n;
        }
    }

    // Tagastab mittenegatiivse arvu (indeksi) kui isik on tabelis, -1 muidu
    Isik otsiKirje(int id){
        int indeks = otsiKirjeIndeks(id);
        if(indeks == -1){
            return null;
        }
        return tabel[indeks];
    }

    int otsiKirjeIndeks(int id){
        // Leian paiskamisega õige koha
        int i = h(id);

        // Et ma ei jääks lõpmatusse tsüklisse
        int samme = 0;

        //Seni kuni leian tühja augu
        while(tabel[i] != null){
            //Vaatan kas tabeli elemendid on mu otsitav
            if(tabel[i].ID == id){
                return i;
            }

            i = (i+kompesamm) % this.n;

            //Kui tabel on täis
            samme++;
            if(samme == this.n){
                return -1;
            }
        }
        //Kui oleme augu leidnud nii et pole otsitavat leidnud
        //Siis teda pole
        return -1;
    }

    int h(int k){
        double T = (Math.sqrt(5)-1)/2;
        return (int) (this.n*(k*T - Math.floor(k*T)));
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Tabel:\n");
        for(int i = 0; i < this.n; i++){
            if(tabel[i] != null){
                sb.append(i+":["+tabel[i]+"]\n");
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) {

        Paisktabel p = new Paisktabel(100,1);

        Isik pt = new Isik(0,"Peregrin Took", 5000, 20);
        Isik mb = new Isik(1, "Meriadoc Brandybuck", 2000, 21);

        p.lisaKirje(pt);
        p.lisaKirje(mb);

        System.out.println(p);
    }
}
