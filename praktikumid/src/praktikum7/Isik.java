package praktikum7;

public class Isik {
    int ID;
    String nimi;
    int palk;
    int vanus;

    public Isik(int ID, String nimi, int palk, int vanus) {
        this.ID = ID;
        this.nimi = nimi;
        this.palk = palk;
        this.vanus = vanus;
    }

    public int getID() {
        return ID;
    }

    public String getNimi() {
        return nimi;
    }

    public int getPalk() {
        return palk;
    }

    public int getVanus() {
        return vanus;
    }

    @Override
    public String toString() {
        return "Isik nr. " + ID + " " + nimi;
    }
}
