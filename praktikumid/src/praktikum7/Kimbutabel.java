package praktikum7;

import java.util.ArrayList;
import java.util.Arrays;

public class Kimbutabel {
    int n;
    ArrayList<Isik>[] tabel;

    public Kimbutabel(int n){
        this.n=n;
        this.tabel = new ArrayList[n];
    }

    void lisaKirje(Isik isik){
        int indeks = h(isik.getID());
        if(tabel[indeks] == null){
            tabel[indeks] = new ArrayList<>();
        }
        tabel[indeks].add(isik);
    }


    // Vali siia sobiv paiskfunktsioon, see peab olema stabiilne
    // i > j => h(i) > h(j)
    int h(int k){
        double T = (Math.sqrt(5)-1)/2;
        return (int) (this.n*(k*T - Math.floor(k*T)));
    }

    public static void main(String[] args) {
        Kimbutabel k = new Kimbutabel(100);


        Isik pt = new Isik(0,"Peregrin Took", 5000, 20);
        Isik mb = new Isik(1, "Meriadoc Brandybuck", 2000, 21);

        k.lisaKirje(pt);
        k.lisaKirje(mb);


        System.out.println(Arrays.toString(k.tabel));
    }
}
