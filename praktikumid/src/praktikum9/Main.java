package praktikum9;

public class Main {
    public static void main(String[] args) {
        Tipp<Integer> puu = new Tipp<Integer>(
                3,
                new Tipp<Integer>(1),
                new Tipp<Integer>(8,
                        new Tipp<Integer>(6),
                        new Tipp<Integer>(13)));
        System.out.println(puu);

        Tipp<Integer> tasakaalustamata = puu(3,
                leht(1),
                puu(6,
                        tyhi(),
                        puu(8,
                                tyhi(),
                                leht(13))));
        System.out.println(tasakaalustamata);

        System.out.println(Integer.compare(0,5));
        System.out.println(Double.compare(5,1));

    }

    /* selliseid mugavusmeetodeid saab teha,
     et konstruktorid ei tee kõike kirjuks */
    static Tipp<Integer> leht(int i){
        return new Tipp<Integer>(i);
    }

    static Tipp<Integer> puu(int info, Tipp<Integer> vasak, Tipp<Integer> parem){
        return new Tipp<Integer>(info,vasak,parem);
    }

    static Tipp<Integer> tyhi(){
        return null;
    }
}
