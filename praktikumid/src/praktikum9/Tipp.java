package praktikum9;

public class Tipp<T extends Comparable<T>> implements Comparable<Tipp<T>> {
    protected T info;
    protected Tipp<T> paremAlluv;
    protected Tipp<T> vasakAlluv;
    protected int sagedus;

    public Tipp(T info, int sagedus) {
        this.info = info;
        this.sagedus = sagedus;
    }

    public Tipp(T info, int sagedus, Tipp<T> paremAlluv, Tipp<T> vasakAlluv) {
        this.info = info;
        this.paremAlluv = paremAlluv;
        this.vasakAlluv = vasakAlluv;
        this.sagedus = sagedus;
    }

    //Kui tahad lisapunkti
    protected int korgus;

    public Tipp(T info, Tipp<T> vasakAlluv, Tipp<T> paremAlluv) {
        this.info = info;
        this.paremAlluv = paremAlluv;
        this.vasakAlluv = vasakAlluv;
    }

    //klassimeetodiga
    public static int tippudeArv(Tipp t){
        if(t==null){
            return 0;
        }
        return 1 + tippudeArv(t.paremAlluv) + tippudeArv(t.vasakAlluv);
    }

    //isendimeetodiga
    public int tippudeArv2(){
        // ternary süntaks: tingimus ? trueVäärtus : falseVäärtus;
        int vasakult = this.vasakAlluv == null ? 0 : vasakAlluv.tippudeArv2();
        int paremalt = this.paremAlluv == null ? 0 : paremAlluv.tippudeArv2();
        return 1 + vasakult + paremalt;
    }

    public static int korgus(Tipp t){
        if(t==null){
            return 0;
        }
        return 1 + Integer.max(korgus(t.vasakAlluv),korgus(t.paremAlluv));
    }

    //eeldan, et on kahendotsimispuu
    public T leiaSuurim(){
        // lähen nii palju paremale kui võimalik
        if(this.paremAlluv == null){
            return this.info;
        }
        return paremAlluv.leiaSuurim();
    }

    //Ilma AVL tasakaalustamiseta
    public Tipp<T> lisaKirjeLihtne(T vaartus){

        if(this.info.compareTo(vaartus) < 0){
            // lähen paremale

            // kontrollin kas parem alampuu on olemas
            if(this.paremAlluv == null){
                this.paremAlluv = new Tipp<T>(vaartus);
            } else {
                //harjume ära nii tegema, eemaldamisel (või tasakaalustamisega) on see väga tähtis!
                this.paremAlluv = this.paremAlluv.lisaKirjeLihtne(vaartus);
            }
        } else {
            //väiksemad ja võrdsed vasakule
            if(this.vasakAlluv==null){
                this.vasakAlluv = new Tipp<T>(vaartus);
            } else {
                this.vasakAlluv = this.vasakAlluv.lisaKirjeLihtne(vaartus);
            }
        }
        //juur siin kunagi ei muutu
        return this;
    }

    /* Kui implementeerid need abimeetodid, siis su elu läheb palju lihtsamaks */
    //Alati tagastame juure!!!
    public Tipp<T> paremPoore(){
        Tipp<T> c = this.vasakAlluv.paremAlluv;
        Tipp<T> uusjuur = this.vasakAlluv;
        this.vasakAlluv.paremAlluv = this;
        this.vasakAlluv = c;
        return uusjuur;
    }

    public Tipp<T> vasakPoore(){
        throw new RuntimeException();
    }

    public Tipp<T> vasakParemPoore(){
        throw new RuntimeException();
    }

    public Tipp<T> paremVasakPoore(){
        throw new RuntimeException();
    }

    /* Kontrollib, kas tipp on tasakaalust väljas, ja teeb pöörde kui vaja */
    public Tipp<T> tasakaalusta(){
        throw new RuntimeException();
    }

    public Tipp(T info) {
        this.info = info;
    }

    public T getInfo(){
        return this.info;
    }

    public Tipp<T> getParemAlluv() {
        return this.paremAlluv;
    }

    public Tipp<T> getVasakAlluv() {
        return this.vasakAlluv;
    }

    public void setParemAlluv(Tipp<T> paremAlluv) {
        this.paremAlluv = paremAlluv;
    }

    public void setVasakAlluv(Tipp<T> vasakAlluv) {
        this.vasakAlluv = vasakAlluv;
    }

    public Tipp<T> lisaKirje(T kirje) {
        throw new RuntimeException();
    }

    public Tipp<T> eemaldaKirje(T kirje) {
        throw new RuntimeException();
    }

    public String sulgEsitus(){
        String paremSone = paremAlluv == null ? "" : paremAlluv.sulgEsitus();
        String vasakSone = vasakAlluv == null ? "" : vasakAlluv.sulgEsitus();
        return info.toString()+"("+vasakSone+")("+paremSone+")";
    }

    @Override
    public String toString() {
        return this.sulgEsitus();
    }

    /* Tegime selle praktikumis 12 et tippe saaks kuhjas kasutada */
    @Override
    public int compareTo(Tipp<T> tTipp) {
        return Integer.compare(this.sagedus,tTipp.sagedus);
    }
}
