package praktikum9;

import org.junit.Test;

import static org.junit.Assert.*;

public class Kodu5Test {

    //Lihtne vasakpööre lisamisel
    @Test
    public void test01(){
        Tipp<Integer> puu = teePuu(1,2,3);
        assertEquals("2(1()())(3()())", puu.sulgEsitus());
    }

    //Lihtne parempööre lisamisel
    @Test
    public void test02(){
        Tipp<Integer> puu = teePuu(3,2,1);
        assertEquals("2(1()())(3()())", puu.sulgEsitus());
    }

    @Test
    public void test03(){
        Tipp<Integer> puu = teePuu(3,6,1,8,13);
        assertEquals("3(1()())(6()(8()(13()())))",puu.sulgEsitus());
    }

    @Test
    public void test04(){
        Tipp<Integer> puu = puu(
                3,
                    puu(2,
                            leht(1),
                            null),
                null);
        puu = puu.paremPoore();
        assertEquals("2(1()())(3()())",puu.sulgEsitus());
    }

    private Tipp<Integer> teePuu(Integer... jarjend){
        Tipp<Integer> puu = null;
        for(Integer i: jarjend){
            puu = puu == null ? new Tipp<Integer>(i) : puu.lisaKirjeLihtne(i);
        }
        return puu;
    }

    static Tipp<Integer> leht(int i){
        return new Tipp<Integer>(i);
    }

    static Tipp<Integer> puu(int info, Tipp<Integer> vasak, Tipp<Integer> parem){
        return new Tipp<Integer>(info,vasak,parem);
    }
}
