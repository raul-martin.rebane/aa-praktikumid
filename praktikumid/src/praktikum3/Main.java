package praktikum3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        //bitivektorid(30,2,"");
        //Vastus v = leiaJaotus(new ArrayList<>(Arrays.asList(52,75,81)),0,new ArrayList<>(), new ArrayList<>());
        //System.out.println(v);

        System.out.println(lahutused(5,new ArrayList<>()));
    }

    public static int lahutused(int arv,List<Integer> liidetavad) {
        if(arv == 0){
            System.out.println(liidetavad);
            return 1;
        }
        if (arv < 0){
            return 0;
        }

        // if(liidetavates ebasobiv muster)
        //   return 0;

        liidetavad.add(1);
        int vasakult = lahutused(arv-1,liidetavad);//lahutused(arv-1,liidetavad+[1]) pythonis ja tasemetöös
        liidetavad.remove(liidetavad.size()-1);

        liidetavad.add(2);
        int paremalt = lahutused(arv-2,liidetavad);
        liidetavad.remove(liidetavad.size()-1);

        return vasakult+paremalt;
    }

    public static int bitivektorid(int pikkus, int yhtesi, String sone) {
        if (sone.length() == pikkus){
            if(yhtesi == 0){
                System.out.println(sone);
                return 1;
            } else {
                return 0;
            }
        } else {
            if (yhtesi < 0){
                return 0;
            }
            return bitivektorid(pikkus,yhtesi,sone+"0")
                    + bitivektorid(pikkus,yhtesi-1,sone+"1");
        }
    }

    public static Vastus leiaJaotus(List<Integer> kaalud, int indeks, List<Integer> jaotus1, List<Integer> jaotus2) {

        if (indeks == kaalud.size()){
            // olen lõppu jõudnud, vormistan vastuse.
            int summa = 0;
            for(Integer i: jaotus1){
                summa += i;
            }
            for(Integer i: jaotus2){
                summa -= i;
            }
            return new Vastus(Math.abs(summa),new ArrayList<>(jaotus1),new ArrayList<>(jaotus2));
        }
        // Väga tähtis! Tee objektidest koopia või lisa-eemalda (ja ole ettevaatlik)
        jaotus1.add(kaalud.get(indeks)); //paneme vasakule selle elemendi
        Vastus v1 = leiaJaotus(kaalud,indeks+1,jaotus1,jaotus2);
        jaotus1.remove(jaotus1.size()-1); //võtame välja

        jaotus2.add(kaalud.get(indeks));
        Vastus v2 = leiaJaotus(kaalud,indeks+1,jaotus1,jaotus2);
        jaotus2.remove(jaotus2.size()-1);

        if(v1.kaaludeVahe < v2.kaaludeVahe){
            return v1;
        } else {
            return v2;
        }
    }
}

/* Pythonis/tasemetöös võite kasutada mitme väärtuse tagastamiseks ennikuid */
class Vastus implements Comparable<Vastus> {
    public int kaaludeVahe;
    public List<Integer> jaotus1;
    public List<Integer> jaotus2;

    public Vastus(int kaaludeVahe, List<Integer> jaotus1, List<Integer> jaotus2) {
        this.kaaludeVahe = kaaludeVahe;
        this.jaotus1 = jaotus1;
        this.jaotus2 = jaotus2;
    }

    @Override
    public int compareTo(Vastus vastus) {
        return Integer.compare(kaaludeVahe,vastus.kaaludeVahe);
    }

    @Override
    public String toString() {
        return "Vastus{" +
                "kaaludeVahe=" + kaaludeVahe +
                ", jaotus1=" + jaotus1 +
                ", jaotus2=" + jaotus2 +
                '}';
    }
}

