package praktikum4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> hinnad = new ArrayList<>(Arrays.asList(70,80,2,5,10));
        System.out.println(yl4(hinnad,0,new ArrayList<>()));

        List<String> a = Arrays.asList("Tere","mina","kala","seen","roheline");
        yl5(a,0,new ArrayList<>());
    }

    public static void yl5(List<String> sisend, int k, List<String> kombinatsioon){
        if(kombinatsioon.size()==3){
            System.out.println(kombinatsioon);
        }
        if(k >= sisend.size()){
            return;
        }

        for(int i=k ; i<sisend.size();i++){
            kombinatsioon.add(sisend.get(i));
            yl5(sisend,i+1,kombinatsioon);
            kombinatsioon.remove(kombinatsioon.size()-1);
        }
        // [tere, kala, siin, seen, roheline], k=3
        /*

        [tere, kala, siin]
        [tere, kala, seen]
        [tere, kala, roheline]

                                    [seen]
                    [kala]          [siin]
        [tere]                      [roheline]

        [seen,kala,tere]

        [kala,tere,...]

         */
    }


    public static List<Integer> yl4(List<Integer> hinnad, int i, List<Integer> alamhulk){
        // baas - oleme indeksiga lõppu jõudnud
        if(i >= hinnad.size()){

            int summa = 0;
            for(Integer j: alamhulk){
                summa += j;
            }
            return new ArrayList<>(Arrays.asList(summa));
        } else {
            //samm - hargneme nii, et kas võtame elemendi alamhulka või ei võta

            // ei võta
            List<Integer> vastus1 = yl4(hinnad,i+1,alamhulk);
            alamhulk.add(hinnad.get(i));
            List<Integer> vastus2 = yl4(hinnad,i+1,alamhulk);
            alamhulk.remove(alamhulk.size()-1);

            List<Integer> lopptulemus = new ArrayList<>();
            int v1 = 0;
            int v2 = 0;

            //Põimesordi põimimine. Kui teete paberil, siis võib järjendid lihtsalt liita ja sorteerimismeetodit kutsuda.
            while(true){
                // vastus1 elemendid otsas, lisan vastus2 järelejäänud
                if(v1 >= vastus1.size()){
                    for(int j=v2; j < vastus2.size(); j++){
                        lopptulemus.add(vastus2.get(j));
                    }
                    break;
                }
                // sama teise poole jaoks
                if(v2 >= vastus2.size()){
                    for(int j=v1; j < vastus1.size(); j++){
                        lopptulemus.add(vastus1.get(j));
                    }
                    break;
                }

                //lisan väiksema elemendi
                if(vastus1.get(v1) < vastus2.get(v2)){
                    lopptulemus.add(vastus1.get(v1));
                    v1++;
                } else if (vastus2.get(v2) < vastus1.get(v1)){
                    lopptulemus.add(vastus2.get(v2));
                    v2++;
                } else { // kui nad on võrdsed, siis kustutan ühe
                    v1++;
                }
            }
            return lopptulemus;
        }
    }

    /*
    Tasemetöö näidise teeme pythonis, kuna seda on paberil parem teha

        from math import floor
    def yl(sendid, i, summa, valitud):
        if summa==0:
            return valitud
        if summa < 0:
            return None
        if i >= len(sendid) and summa > 0:
            return None
        if i < len(sendid):
            #hargneme yhe sendiga nii palju kui saab.
            hargnemisi = 10/sendid[i]
            vastused = []
            for arv in range(0,floor(hargnemisi)):
                v = yl(sendid,i+1,summa-arv*sendid[i],valitud+arv)
                vastused.append(v)
            parim = None
            for vastus in vastused:
                if vastus:
                    if parim == None or vastus < parim:
                        parim = vastus
            return parim

    sendid = [0.10, 0.20,0.50]
    print(yl(sendid,0,5.70,0))

     */
}
