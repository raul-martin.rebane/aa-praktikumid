package praktikum5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class Tasemetoo {

    public static void main(String[] args) {
        List<Double> hinnad = new ArrayList<>(Arrays.asList(6.0,9.0,19.0));
        ylesanne(hinnad);
    }

    public static Double ylesanne(List<Double> hinnad){
        HetkeSeis algseis = new HetkeSeis(0,0.0);
        Stack<HetkeSeis> stack = new Stack();
        stack.add(algseis);

        Double parimSeis = null;

        while(!stack.empty()){
            System.out.println(stack);
            HetkeSeis seis = stack.pop();

            if(seis.valitudKaal > 100){
                //lootusetu haru, ei hargne
                continue;
            }
            else if(seis.indeks >= hinnad.size()){
                // Oleme lõppu jõudnud, kas see on meie parim seis?
                if(parimSeis == null || seis.valitudKaal > parimSeis){
                    parimSeis = seis.valitudKaal;
                }
            } else {
                //ei võta elementi
                stack.add(new HetkeSeis(seis.indeks+1, seis.valitudKaal));
                //võtame indeksil oleva 1 korra
                stack.add(new HetkeSeis(seis.indeks+1, seis.valitudKaal+hinnad.get(seis.indeks)));
                //võtame indeksil oleva 2 korda
                stack.add(new HetkeSeis(seis.indeks+1, seis.valitudKaal+2*hinnad.get(seis.indeks)));
            }
        }
        return parimSeis;
    }
}

class HetkeSeis {
    int indeks;
    double valitudKaal;

    public HetkeSeis(int indeks, double valitudKaal) {
        this.indeks = indeks;
        this.valitudKaal = valitudKaal;
    }

    @Override
    public String toString() {
        return "("+this.indeks+';'+this.valitudKaal+")";
    }
}