package praktikum5;

import java.util.Arrays;
import java.util.Stack;

public class Sorteerimine {
    public static void main(String[] args) {
        int[] blaa = {4,5,6,10,3,4,7,1,2,90,50};
        kiirsortMagasiniga(blaa);
        System.out.println(Arrays.toString(blaa));
    }

    public static void kiirsortMagasiniga(int[] m){
        Stack<SorteerimiseSeis> stack = new Stack();
        stack.add(new SorteerimiseSeis(0,m.length-1));
        while(!stack.empty()){
            SorteerimiseSeis hetkeSeis = stack.pop();

            /*
            System.out.println("Vaatan ala: "+hetkeSeis.algus+";"+hetkeSeis.lopp);
            System.out.println(Arrays.toString(m));
            */

            int vaadeldavaAlaSuurus = hetkeSeis.algus - hetkeSeis.lopp;
            if(vaadeldavaAlaSuurus == 0 || vaadeldavaAlaSuurus == 1){
                continue;
            }
            int suureAlgus = teeLahe(m,hetkeSeis.algus,hetkeSeis.lopp);

            stack.add(new SorteerimiseSeis(hetkeSeis.algus,suureAlgus));
            stack.add(new SorteerimiseSeis(suureAlgus+1,hetkeSeis.lopp));
        }
    }

    public static int teeLahe(int[] m, int algus, int lopp){
        int j = algus - 1;
        int k = lopp + 1;
        int lahe = m[(algus+lopp)/2];
        while (j != k){
            //tulen vasakult, otsin esimest liiga suurt elementi
            do{
                j++;
            } while (m[j] < lahe);
            //tulen paremalt, otsin esimest liiga väikest elementi
            do {
                k--;
            } while (m[k] > lahe);

            //kui nad on teineteisest möödunud, oleme lõpetanud.
            if(j >= k){
                return k;
            }

            //vahetan ära
            int t = m[j];
            m[j] = m[k];
            m[k] = t;
        }
        return k;
    }
}

class SorteerimiseSeis {
    int algus;
    int lopp;

    public SorteerimiseSeis(int algus, int lopp) {
        this.algus = algus;
        this.lopp = lopp;
    }
}
