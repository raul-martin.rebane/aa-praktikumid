package praktikum6;

import praktikum6.avaldis.LauseArvutuseAvaldis;

import java.util.Stack;

public class Lausearvutus {
    public static void main(String[] args) {
        // A&¬B
        System.out.println(vaartusta("A B ¬ &"));
        System.out.println(vaartusta(postfixKujule("A B ¬ &")));
    }

    // A&B -> and(A,B)
    // ¬A -> not(A)


    // lihtsustus: ma saan pööratud poola kujul valemi
    // lihtsustus: sümbolite/tokenite vahel tühikud
    // lihtsustus: sulge pole


    public static String postfixKujule(String s){
        //koostejaamameetodiga teeme infix -> pööratud poola kujule (postfix)
        return null;
    }

    /*
    See meetod on niisama illustreerimiseks, et tegelikult kui sa suudad lahendada postfixkujule viimise
    ülesande, siis sa tegelikult suudad ka viia avaldise puu kujule. Kui see tundub segadusse ajav,
    siis võid julgelt ignoreerida seda :).
     */
    public static LauseArvutuseAvaldis puuKujule(String s){
        return null;
    }

    // A & B v C -> A B C & v

    public static String vaartusta(String valem){
        String[] elemendid = valem.split(" ");
        Stack<String> vaheTulemused = new Stack<>();

        // Sama nagu aritmeetilise avaldise puhul
        for(String s: elemendid){
            //kas on tehe või väärtus
            if(onTehe(s)){
                // sooritame selle tehte, olenevalt operandide arvust
                if(s.equals("¬")){
                    String opereeritav = vaheTulemused.pop();
                    vaheTulemused.add("not("+opereeritav+")");
                } else {
                    String paremal = vaheTulemused.pop();
                    String vasakul = vaheTulemused.pop();

                    String tehe = "";
                    if(s.equals("&")){
                        tehe = "and";
                    }
                    if(s.equals("v")){
                        tehe = "or";
                    }
                    vaheTulemused.add(tehe+"("+vasakul+","+paremal+")");
                }
            } else {
                //literaalid magasini
                vaheTulemused.add(s);
            }
        }
        return vaheTulemused.pop();
    }


    private static boolean onTehe(String s) {
        return s.equals("¬") || s.equals("&") || s.equals("v");
    }
}
