package praktikum6.avaldis;

public class Muutuja extends LauseArvutuseAvaldis{
    protected String nimi;

    public Muutuja(String nimi){
        this.nimi = nimi;
    }
    @Override
    public boolean vaartusta() {
        return false;
    }

    @Override
    public String ylVastus() {
        return null;
    }

    @Override
    protected String soneks(LauseArvutuseAvaldis kontekst) {
        return this.nimi;
    }


    @Override
    public String toString() {
        return this.nimi;
    }
}
