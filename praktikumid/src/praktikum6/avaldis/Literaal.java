package praktikum6.avaldis;

public class Literaal extends LauseArvutuseAvaldis {
    boolean vaartus;


    @Override
    public boolean vaartusta() {
        return vaartus;
    }

    public Literaal(boolean vaartus) {
        this.vaartus = vaartus;
    }

    @Override
    public String ylVastus() {
        return Boolean.toString(vaartus);
    }

    @Override
    protected String soneks(LauseArvutuseAvaldis kontekst) {
        return Boolean.toString(vaartus);
    }
}
