package praktikum6.avaldis;

public class Eitus extends LauseArvutuseAvaldis {
    LauseArvutuseAvaldis laps;

    @Override
    public boolean vaartusta() {
        return !laps.vaartusta();
    }

    public Eitus(LauseArvutuseAvaldis laps) {
        this.laps = laps;
    }

    @Override
    public String ylVastus() {
        return "not("+laps.vaartusta()+")";
    }

    @Override
    protected String soneks(LauseArvutuseAvaldis kontekst) {
        return "!"+laps.soneks(this);
    }

    @Override
    public String toString() {
        return "!("+laps+")";
    }
}
