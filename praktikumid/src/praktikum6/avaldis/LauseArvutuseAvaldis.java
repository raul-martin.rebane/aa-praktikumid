package praktikum6.avaldis;

/*
    Need klassid on lihtsalt illustreerimaks, et peamine asi on saada
    tehte struktuur valemist kätte, ülejäänud on lihtne
 */
public abstract class LauseArvutuseAvaldis {
    public abstract boolean vaartusta();

    public abstract String ylVastus();

    protected abstract String soneks(LauseArvutuseAvaldis kontekst);
}
