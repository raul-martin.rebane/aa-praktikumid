package praktikum6.avaldis;

public class Konjunktsioon  extends LauseArvutuseAvaldis{
    LauseArvutuseAvaldis vasak;
    LauseArvutuseAvaldis parem;

    @Override
    public boolean vaartusta() {
        return vasak.vaartusta() && parem.vaartusta();
    }

    public Konjunktsioon(LauseArvutuseAvaldis vasak, LauseArvutuseAvaldis parem) {
        this.vasak = vasak;
        this.parem = parem;
    }

    @Override
    public String ylVastus() {
        return "and("+vasak.ylVastus()+","+parem.ylVastus()+")";
    }

    @Override
    protected String soneks(LauseArvutuseAvaldis kontekst) {
        boolean sulge = false;
        if(kontekst instanceof Eitus){
            sulge = true;
        }
        String vasakuSone = vasak.soneks(this);
        String paremaSone = parem.soneks(this);
        if(sulge){
            return "("+vasakuSone+"&"+paremaSone+")";
        }
        return vasakuSone + "&" + paremaSone;
    }

    @Override
    public String toString() {
        return soneks(this);
    }
}
