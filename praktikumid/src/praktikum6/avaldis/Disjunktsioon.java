package praktikum6.avaldis;

public class Disjunktsioon extends LauseArvutuseAvaldis {
    LauseArvutuseAvaldis vasak;
    LauseArvutuseAvaldis parem;

    @Override
    public boolean vaartusta() {
        return vasak.vaartusta() || parem.vaartusta();
    }

    public Disjunktsioon(LauseArvutuseAvaldis vasak, LauseArvutuseAvaldis parem) {
        this.vasak = vasak;
        this.parem = parem;
    }

    @Override
    public String ylVastus() {
        return "or("+vasak.ylVastus()+","+parem.ylVastus()+")";
    }

    @Override
    public String toString() {
        return "(" +vasak + "v" + parem + ")";
    }

    protected String soneks(LauseArvutuseAvaldis vanem){
        boolean sulge = false;
        if(vanem instanceof Eitus || vanem instanceof Konjunktsioon){
            sulge = true;
        }

        String vasakuSone = vasak.soneks(this);
        String paremaSone = parem.soneks(this);
        if(sulge){
            return "(" + vasakuSone + "v" + paremaSone + ")";
        }
        return  vasakuSone + "v" + paremaSone;
    }
}
