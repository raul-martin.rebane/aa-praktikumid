package praktikum12;

import java.io.*;
import java.util.*;

public class Lahendaja {

    //Linnadevahelised kaugused
    public int[][] kaugused;

    //Sisaldab linnade nimesid
    //Saab kasutada nii rea kui tulba nime leidmiseks
    public String [] nimed;
    public HashMap<String,Integer> linnaIndeksid;

    //floyd warshalli teede tabel
    public String[][] kustTulla;

    public Lahendaja(String fail) throws FileNotFoundException {
        loeKaugusedFailist(fail);
    }

    //See on Floyd-Warshalli jaoks eraldi konstruktor, et saame
    //osad servad välja lülitada
    public Lahendaja(int[][] kaugused, String[] nimed, int maxKaal){
        this.nimed = new String[nimed.length];
        for(int i=0;i<nimed.length;i++){
            this.nimed[i] = nimed[i];
        }

        this.linnaIndeksid = new HashMap<>();
        for(int i=0;i<nimed.length;i++){
            linnaIndeksid.put(nimed[i],i);
        }

        this.kaugused = new int[kaugused.length][kaugused.length];
        this.kustTulla = new String[kaugused.length][kaugused.length];
        for(int i=0;i<kaugused.length;i++){
            for(int j=0;j<kaugused.length;j++){
                int kaal = kaugused[i][j];
                if(kaal >= maxKaal){
                    this.kaugused[i][j] = Integer.MAX_VALUE/2;
                } else {
                    this.kaugused[i][j] = kaal;
                    this.kustTulla[i][j] = nimed[i];
                }
            }
        }

    }

    public void floydWarshall(){
        for(int k=0;k<kaugused.length;k++){
            for(int a = 0; a < kaugused.length;a++){
                for(int l = 0; l < kaugused.length; l++){
                    if(kaugused[a][k] + kaugused[k][l] < kaugused[a][l]) {
                        kaugused[a][l] = kaugused[a][k] + kaugused[k][l];
                        this.kustTulla[a][l] = this.kustTulla[k][l];
                    }
                }
            }
        }
    }

    public Vastus lyhimTeeFloydWarshalliga(String algus, String lõpp){
        floydWarshall();
        if(this.kustTulla[indeks(algus)][indeks(lõpp)] == null){
            return new Vastus(new ArrayList<>(Arrays.asList(algus,lõpp)),-1);
        }

        List<String> teekond = new ArrayList<>();
        String viimane = lõpp;
        do {
            teekond.add(0,viimane);
            viimane = this.kustTulla[indeks(algus)][indeks(viimane)];
        } while(!viimane.equals(algus));
        teekond.add(0,algus);
        int teepikkus = this.kaugused[indeks(algus)][indeks(lõpp)];
        return new Vastus(teekond,teepikkus);
    }

    /**
     * Leiab lühima tee linnade 'algus' ja 'lõpp' vahel.
     * @param algus Alguslinn sõnena
     * @param lõpp Lõpplinn sõnena
     * @param max Maksimaalne kaugus, mida auto saab ühe tankimisega läbida
     * (ülesande tekstis tähistatud kui x)
     * @return Vastuse isend, mis sisaldab leitud lühimat teed ja teepikkust
     * Kui vastust ei leidu (algus/lõpp ebasobiv, ei saa läbida sellise 'max'ga),
     * tagastada kodu7.Vastus teepikkusega -1.
     */
    public Vastus leiaLühimTee(String algus, String lõpp, int max) {
        //TODO: Lahendada Dijkstra või Bellman-Ford algoritmiga

        //Dijkstra algoritm on õpikus lehekülg 100.
        //Soovituslikult implementeerida algoritm kasutades naabrusmaatriksit,
        //kuid võite lisada ka Tipp ja Serv klassid ning failist lugemist
        //selleks sobivaks töödelda

        //alguses Integer.INT_MAX/2;
        int[] kaugusAlgusest = new int[kaugused.length];
        int[] kustTulla = new int[kaugused.length];

        // kui kuhja ei saa/suuda kasutada, siis võta kauguste tabelist
        // seni vaatamata elementidest miinimumi.
        // Sel juhul on vaja ka vaadatud tippe meelde jätta
        // HashMap<Integer,Boolean> vaadatud = new HashMap<>();

        /*
        A->D   A->C->E->B->D
        |A|B|C|D|E|
        |-|E|A|B|C|
         */


        return new Vastus(Arrays.asList(algus,lõpp),-1);
    }

    // Praktikumis soojendusharjutus. Prindib välja kõik tipud, kuhu on võimalik linnast "algus" minna, kasutades
    // ainult maksimaalselt "max" kaaluga kaari.
    public void sihtkohad(String algus, int max){
        // Printida välja kõik linnad kuhu on võimalik alguslinnast minna
        // Kasutada võib ainult kaari mis on <= max
        List<Integer> järjekord = new ArrayList<>();
        järjekord.add(indeks(algus));
        HashMap<Integer,Boolean> vaadatud = new HashMap<>();
        while(!järjekord.isEmpty()){
            int vaadeldav = järjekord.remove(0);
            System.out.println(nimed[vaadeldav]);
            vaadatud.put(vaadeldav,true);

            for(int i=0;i<kaugused.length;i++){
                if(kaugused[vaadeldav][i] <= max && vaadatud.getOrDefault(i,false) == false){
                    vaadatud.put(i,true);
                    järjekord.add(i);
                }
            }
        }
    }

    // maxKaugus = võin läbida maksimaalselt km
    // maxKaareKaal = võin läbida ühes sammus maksimaalselt nii palju km
    public void raadius(String algus, int maxKaugus, int maxKaareKaal){

        List<Retkepaar> järjekord = new ArrayList<>();
        HashMap<Integer,Boolean> käidud = new HashMap<>();
        käidud.put(indeks(algus),true);
        järjekord.add(new Retkepaar(indeks(algus),maxKaugus,käidud));

        int parimKaugus = 0;
        int parimLinn = 0;

        while(!järjekord.isEmpty()){
            Retkepaar vaadeldav = järjekord.remove(0);
            boolean hargnenud = false;
            for(int i=0;i<kaugused.length;i++){
                int linnaKaugus = kaugused[vaadeldav.praeguneLinn][i];
                if(linnaKaugus <= maxKaareKaal &&
                        vaadeldav.kaugustJärel >= linnaKaugus &&
                        vaadeldav.käidud.getOrDefault(i,false)==false){
                    HashMap<Integer,Boolean> uueRetkeMap = (HashMap<Integer,Boolean>) vaadeldav.käidud.clone();
                    uueRetkeMap.put(i,true);
                    järjekord.add(new Retkepaar(i,vaadeldav.kaugustJärel - linnaKaugus, uueRetkeMap));
                    hargnenud = true;
                }
            }
            if(!hargnenud){
                if(kaugused[indeks(algus)][vaadeldav.praeguneLinn] > parimKaugus){
                    parimLinn = vaadeldav.praeguneLinn;
                }
            }
        }
        System.out.println("Kõige kaugemal jõudsin: " + nimed[parimLinn] + " otsekaugus: " + kaugused[indeks(algus)][parimLinn]);
    }

    /*
    Mugavusmeetod, mis võtab linna nime ning tagastab temale vastava indeksid massiivides "kaugused" ja "nimed".
     */
    public int indeks(String linn){
        return linnaIndeksid.get(linn);
    }

    private void loeKaugusedFailist(String fail) throws FileNotFoundException {
        try (BufferedReader br = new BufferedReader(new FileReader(new File(fail)))){
            nimed = br.readLine().split("\t");
            kaugused = new int[nimed.length][nimed.length];
            linnaIndeksid = new HashMap<>();
            for(int i=0; i < nimed.length; i++){
                linnaIndeksid.put(nimed[i],i);
            }
            String in = br.readLine();
            int i = 0;
            while(in != null){
                String[] reaJupid = in.split("\t");
                for(int j=1;j<reaJupid.length;j++){
                    if(!reaJupid[j].equals("")){
                        kaugused[i][j-1] = Integer.parseInt(reaJupid[j]);
                    } else {
                        kaugused[i][j-1] = 0;
                    }
                }
                i++;
                in = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class Retkepaar {
    int praeguneLinn;
    int kaugustJärel;
    HashMap<Integer,Boolean> käidud;

    public Retkepaar(int praeguneLinn, int kaugustJärel, HashMap<Integer, Boolean> käidud) {
        this.praeguneLinn = praeguneLinn;
        this.kaugustJärel = kaugustJärel;
        this.käidud = käidud;
    }
}