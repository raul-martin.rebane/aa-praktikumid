package praktikum12;

import praktikum9.Tipp;

import java.util.ArrayList;
import java.util.List;

public class TipuSonestaja {
    public static void main(String[] args) {
        Tipp<Integer> puu = tipp(
                leht(),
                tipp(leht(),
                        leht()));
        System.out.println(puu);
        System.out.println(puuSoneks(puu));
        System.out.println(sonePuuks("30300"));
    }

    public static String puuSoneks(Tipp<Integer> t){
        List<Tipp<Integer>> järjekord = new ArrayList<>();
        järjekord.add(t);
        StringBuilder struktuurisõne = new StringBuilder();
        while(!järjekord.isEmpty()){
            Tipp<Integer> vaadeldav = järjekord.remove(0);
            if(vaadeldav == null){
                continue;
            }
            struktuurisõne.append(kategoriseeri(vaadeldav));
            järjekord.add(vaadeldav.getVasakAlluv());
            järjekord.add(vaadeldav.getParemAlluv());
        }
        return struktuurisõne.toString();
    }

    public static Tipp<Integer> sonePuuks(String struktuurisõne){
        List<Tipp<Integer>> järjekord = new ArrayList<>();
        Tipp<Integer> juur = leht();
        järjekord.add(juur);
        int i = 0;
        while(!järjekord.isEmpty()){
            Tipp<Integer> vaadeldav = järjekord.remove(0);
            char c = struktuurisõne.charAt(i);
            if(c == '1' || c == '3'){
                Tipp<Integer> v = leht();
                vaadeldav.setVasakAlluv(v);
                järjekord.add(v);
            }
            if(c == '2' || c == '3'){
                Tipp<Integer> p = leht();
                vaadeldav.setParemAlluv(p);
                järjekord.add(p);
            }
            i++;
        }
        return juur;
    }

    public static String kategoriseeri(Tipp<Integer> t){
        if(t.getVasakAlluv() == null && t.getParemAlluv() == null){
            return "0";
        }
        if(t.getParemAlluv() == null){
            return "1";
        }
        if(t.getVasakAlluv() == null){
            return "2";
        }
        return "3";
    }


    public static Tipp<Integer> tipp(Tipp<Integer> vasak, Tipp<Integer> parem){
        return new Tipp<Integer>(0,vasak,parem);
    }

    public static Tipp<Integer> leht(){
        return new Tipp<Integer>(0);
    }
}
